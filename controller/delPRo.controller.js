//delete product
delPro = (product_id) => {
    let proIDD = { pro_id: product_id };
    let delProduct = $.ajax({
        type: 'POST',
        url: "http://localhost/backend/api/deleteproduct",
        contentType: 'application/json',
        data: JSON.stringify(proIDD)
    });
    delProduct.done(data => {
        if (data.msg == "product deleted successfully") {
            let prodelID = "#" + product_id;
            $(prodelID).css('display', 'none');
        } else {
            alert("Xóa sản phẩm lỗi!");
        }
    });
    delProduct.fail((XHR, statuscode) => {
        console.log(statuscode);
    })
};