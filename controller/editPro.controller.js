let productName = $("#productName");
let saleprice = $("#saleprice");
let purchaseprice = $("#purchaseprice");
let editcategories = $("#categories");
let quantity = $("#quantity");
let href = window.location.href;
const idReg = /pro_id/;
let pro_id = href.slice(idReg.exec(href).index + 7);

//Get detail product info
let getAPro = $.get({
    url: "http://localhost/backend/api/allproducts",
    data: {
        id: pro_id
    }
});

getAPro.done(data => {
    productName.val(data.body[0].pro_name);
    saleprice.val(data.body[0].pro_saleprice);
    purchaseprice.val(data.body[0].pro_purchaseprice);
    categories.val(data.body[0].pro_categories);
    quantity.val(data.body[0].pro_quantity);
});

getAPro.fail(() => {
    console.log("Fail to get data");
});

//update products
editProducts = () => {
    if (productName.val().trim() == "" || saleprice.val().trim() == "" || purchaseprice.val().trim() == "" || quantity.val().trim() == "") {
        alert("Nhập đầy đủ các trường dữ liệu");
    } else {
        let editedData = {
            pro_id: pro_id,
            pro_name: productName.val(),
            pro_quantity: quantity.val(),
            pro_saleprice: saleprice.val(),
            pro_purchaseprice: purchaseprice.val()
        };
        let editPro = $.ajax({
            type: 'POST',
            url: "http://localhost/backend/api/updateproduct",
            contentType: 'application/json',
            data: JSON.stringify(editedData)
        });

        editPro.done(data => {
            (data.msg == "Edit products successful") ? window.location.href = "/Products/products.html": alert("Cập nhật sản phẩm lỗi!");
        });

        editPro.fail((jqXHR, textStatus, errorThrown) => {
            console.log("failed");
        });
    }
    return false;
}