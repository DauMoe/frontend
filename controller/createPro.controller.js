let productName = $("#productName");
let saleprice = $("#saleprice");
let purchaseprice = $("#purchaseprice");
let getcategories = $("#categories");
let quantity = $("#quantity");

createProducts = () => {
    // check input fields
    if (productName.val().trim() == "" || saleprice.val().trim() == "" || purchaseprice.val().trim() == "" || categories.val().trim() == "none" || quantity.val().trim() == "") {
        alert("Nhập đầy đủ các trường dữ liệu");
    } else {
        let newPro = {
            pro_name: productName.val(),
            pro_quantity: quantity.val(),
            pro_categories: getcategories.val(),
            pro_saleprice: saleprice.val(),
            pro_purchaseprice: purchaseprice.val()
        };
        let createPro = $.ajax({
            type: 'POST',
            url: "http://localhost/backend/api/createproduct",
            contentType: 'application/json',
            data: JSON.stringify(newPro)
        });

        createPro.done(data => {
            (data.msg == "Create products successful") ? window.location.href = "/Products/products.html": alert("Tạo sản phẩm lỗi!");;
        });

        createPro.fail((jqXHR, textStatus, errorThrown) => {
            alert("Tạo sản phẩm bị lỗi!")
                // console.log("failed");
        });
    }
    return false;
};