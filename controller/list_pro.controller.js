let productList = $("#productList");
let createTag = (item) => {
    return (
        `<tr id="${item.pro_id}">
            <th scope="row">${item.pro_id}</th>
            <td><a href="/Products/productEdit.html?pro_id=${item.pro_id}">${item.pro_name}</a></td>
            <td>${item.pro_categories}</td>
            <td>${item.pro_purchaseprice}</td>
            <td>${item.pro_quantity}</td>
            <td><button class="btn btn-warning" onclick=delPro("${item.pro_id}")>Xóa</button></td>
        </tr>`
    );
};

//Products list controller
let productsList = $.get({
    url: "http://localhost/backend/api/allproducts",
});

productsList.done(data => {
    data.body.map(item => {
        productList.append(createTag(item));
    });
});

productsList.fail((jqXHR, textStatus, errorThrown) => {
    console.log("failed");
})